<?php

/**
 * DatabaseExceptionController Class
 */
class DatabaseExceptionController extends Exception
{
    const CODE_DUPLICATE_ENTRY = 1062;
    const CODE_CONNECTION_DENIED = 2002;
    const CODE_ON_CASCADE_DENIED = 1451;

    public function __construct($message, $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    private function getNewMessage($code)
    {
        switch ($code)
        {
            case self::CODE_DUPLICATE_ENTRY: $msg = "Ya existe este campo en la base de datos.";
                break;
            case self::CODE_CONNECTION_DENIED: $msg = "No se ha podido conectar con la base de datos.";
                break;
            case self::CODE_ON_CASCADE_DENIED: $msg = "No se puede eliminar ya que el modo en cascada no está habilitado.";
                break;
            default: $msg = "Error.";
        }

        return $msg;
    }

    public function __toString()
    {
        return $this->getNewMessage($this->code);
    }
}
