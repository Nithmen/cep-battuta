$.fn.puzzle = function () {
    var $elem = $(this);

    var Piece = function (id, path) {
        this.img = path;
        this.id = id;

        this.getPath = function () {
            return this.img;
        };
        
        this.getId = function()
        {
            return this.id;
        };
    };

    var Puzzle = function () {
        this.pieces = [];

        this.createPuzzle = function ()
        {
            for (var i = 0; i < 25; i++)
            {
                this.pieces[i] = new Piece(i,'../../public/img/puzzles/' + i + '.jpeg');
            }
        };
    };

    function shuffle(array) {
        var i = 0, j = 0, temp = null;

        for (i = array.length - 1; i > 0; i--) {
            j = Math.floor(Math.random() * (i + 1));
            temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
    }

    var puzzle = new Puzzle();
        puzzle.createPuzzle();
        shuffle(puzzle.pieces);

    function createDivs()
    {
        var content = '';

        for (var i in puzzle.pieces)
        {
            //We made a shuffle so we have to identify the class with the image id (name).
            var index = puzzle.pieces[i].getId();
            content += '<div class="piece ui-widget-content" data-value="' + index + '"> <img src="' + puzzle.pieces[i].getPath() + '"> </div>';
        }

        $elem.append('<div class="pieces">' + content + '</div>');
        
        $(".piece").draggable();
    };

    function createGrid()
    {
        var content = '<div class="grid ui-widget-header">';
        
        for (var i in puzzle.pieces)
        {
            content += '<div class="piece-grid" data-target="' + i + '"></div>';
        }
        
        content += '</div>';
         
        $elem.append(content);
    }

    function createSnackbar(message)
    {
        var snackbar = '<div class="snackbar show">' + message + '</div>';
        
        $elem.append(snackbar);
        
        setTimeout(function(){
            $(".snackbar").removeClass('show');
        }, 3000);
    };
    
    function getCode()
    {
        for(var i=10; i < 15; i++)
        {
            $('div[data-target="' + i + '"] img').fadeOut(300, function(){
                $(this).remove();
            });
        }
        
        $('div[data-target="10"]').fadeIn(400, function(){
            $(this).text("SA");
        });
        $('div[data-target="11"]').fadeIn(400, function(){
            $(this).text("BI");
        });
        $('div[data-target="12"]').fadeIn(400, function(){
            $(this).text("DU");
        });
        $('div[data-target="13"]').fadeIn(400, function(){
            $(this).text("RI");
        });
        $('div[data-target="14"]').fadeIn(400, function(){
            $(this).text("A");
        });
    }
    
    
    function createStory()
    {
        var story = "Ibn Battuta va recorrer tota l'Àsia buscant treball com a traductor. Durant el seu viatge va conèixer a persones com el  Khan de la Horda Daurada. Després d'un any va arribar a aquesta ciutat, podries esbrinar quina és?";
        
        $elem.append('<div class="row"><div class="col-md-offset-3 col-md-5"><div class="story">' + story + '</div></div></div>');
    };
    
    createStory();
    
    createGrid();
    createDivs();
 
    $(".piece-grid").droppable({
        drop: function(event, ui){
            //Position de la celda donde hemos puesto la pieza.
            var position = $(this).attr('data-target');
            var pieceValue = ui['draggable'].attr('data-value');

            if(position === pieceValue)
            {
                var img = $('div[data-value="' + position +'"] img')[0];
                this.append(img);
                $('div[data-value="' + position +'"]').remove();
                
                if($('.piece').length === 0)
                { 
                    $('.pieces').remove();
                    createSnackbar("¡Puzzle completado!");
                    getCode();
                }
            }
        }
    });
    
    
    //Pista
    setTimeout(function(){
        
    }, 3000);
};