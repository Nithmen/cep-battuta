function createSnackbar(message,time = 7000, $elem = $(".content"))
{
    var snackbar = '<div class="snackbar show">' + message + '</div>';
    
    $elem.append(snackbar);
    
    setTimeout(function(){
        $(".snackbar").removeClass('show');
    }, time);
};