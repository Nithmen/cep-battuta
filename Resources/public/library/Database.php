<?php

include_once $_SERVER['DOCUMENT_ROOT']. '/cep-battuta/Entity/Role.php';

/**
 * Database Class
 */
class Database
{
    private static $servername = 'localhost';
    private static $db = 'enigmes';
    private static $username = 'root';
    private static $password = null;
    private static $conn = null;
    private static $connected = FALSE;

    /**
     * Connectarse a la base de datos.
     * 
     * @return DatabaseExceptionController
     */
    private static function connect()
    {
        if (!self::isConnected())
        {
            try
            {
                self::$conn = new PDO('mysql:host=' . self::$servername . ';dbname=' . self::$db . ';charset=UTF8', self::$username, self::$password);
                self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                self::$connected = TRUE;
            } catch (PDOException $ex)
            {
                $_SESSION['error'] = new DatabaseExceptionController($ex->getMessage(), $ex->errorInfo[1]);
            }
        }

        return self::$conn;
    }

    /**
     * Desconectar la conexión.
     */
    private static function disconnect()
    {
        self::$conn = null;
        self::$connected = FALSE;
    }

    /**
     * Mirar si la conexión está abierta.
     * 
     * @return boolean $connected
     */
    public static function isConnected()
    {
        return self::$connected;
    }
    
    /**
     * Obtenir tots els usuaris registrats.
     * 
     * @return array
     */
    public static function getUsers()
    {
        $conn = self::connect();
        
        var_dump($conn);
        
        $users = array();
        
        $stmt = $conn->query('SELECT * FROM usuaris');
        
        while($row = $stmt->fetch(PDO::FETCH_ASSOC))
        {
            $users[] = new User($row['id'], $row['nom'], $row['contrasenya'], $row['correu'], $row['token'], $row['terminals_id'], $row['rols_id']);
        }
        
        $conn = self::disconnect();
        
        return $users;
    }
    
    public static function addUser($username, $email, $password)
    {
        $conn = self::connect();

        try
        {
            $stmt = $conn->prepare("INSERT INTO usuaris(nom, contrasenya, correu, rols_id) VALUES (:nom, :contrasenya, :correu, :rols_id)");
            $stmt->bindParam(':nom', $username);
            $stmt->bindValue(':contrasenya', md5($password));
            $stmt->bindParam(':correu', $email);
            $stmt->bindValue(':rols_id', Role::ROLE_USER);
            
            $stmt->execute();
        } catch (PDOException $ex)
        {
            $_SESSION['error'] = new DbException($ex->getMessage(), $ex->errorInfo[1]);
        }

        $conn = self::disconnect();
    }
    
//
//    /**
//     * Devuelve todas las secciones registradas.
//     * 
//     * @return array $secciones
//     */
//    public static function getSecciones()
//    {
//        $conn = self::connect();
//
//        $secciones = array();
//
//        $stmt = $conn->query("SELECT * FROM secciones");
//
//        while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
//        {
//            $secciones[] = array(
//                'id' => $row['id_seccion'],
//                'descripcion' => $row['desc_seccion']
//            );
//        }
//
//        $conn = self::disconnect();
//
//        return $secciones;
//    }
//
//    /**
//     * Buscar una sección por su clave primaria.
//     * 
//     * @param int $id
//     * @return array
//     */
//    public static function getSeccionById($id)
//    {
//        $conn = self::connect();
//
//        $stmt = $conn->prepare("SELECT * FROM secciones where id_seccion = :seccion");
//        $stmt->bindParam(':seccion', $id);
//
//        $stmt->execute();
//
//        $res = $stmt->fetch(PDO::FETCH_ASSOC);
//
//        $conn = self::disconnect();
//
//        return $res['desc_seccion'];
//    }
//
//    /**
//     * Añadir una sección.
//     * 
//     * @param type $description
//     * @throws PDOException
//     */
//    public static function addSection($description)
//    {
//        $conn = self::connect();
//
//        try
//        {
//            $stmt = $conn->prepare("INSERT INTO secciones(desc_seccion) VALUES (:description)");
//            $stmt->bindParam(':description', $description);
//
//            $stmt->execute();
//        } catch (PDOException $ex)
//        {
//            $_SESSION['error'] = new DbException($ex->getMessage(), $ex->errorInfo[1]);
//        }
//
//        $conn = self::disconnect();
//    }
//
//    /**
//     * Modificar una sección.
//     * 
//     * @param type $id
//     * @param type $description
//     * @throws PDOException
//     */
//    public static function editSection($id, $description)
//    {
//        $conn = self::connect();
//
//        try
//        {
//            $stmt = $conn->prepare("UPDATE secciones SET desc_seccion = :description WHERE id_seccion = :section");
//            $stmt->bindParam(':description', $description);
//            $stmt->bindParam(':section', $id);
//
//            $stmt->execute();
//        } catch (PDOException $ex)
//        {
//            $_SESSION['error'] = new DbException($ex->getMessage(), $ex->errorInfo[1]);
//        }
//
//        $conn = self::disconnect();
//    }
//
//    /**
//     * Eliminar una sección.
//     * 
//     * @param type $id
//     * @throws Exception
//     */
//    public static function deleteSection($id)
//    {
//        $conn = self::connect();
//
//        try
//        {
//
//            $stmt = $conn->prepare("DELETE FROM secciones WHERE id_seccion = :section");
//            $stmt->bindParam(':section', $id);
//
//            $stmt->execute();
//        } catch (PDOException $ex)
//        {
//            $_SESSION['error'] = new DbException($ex->getMessage(), $ex->errorInfo[1]);
//        }
//
//        $conn = self::disconnect();
//    }

}
