<?php

include "../default/layout.php";
?>

<?php startblock('stylesheet') ?>
<link rel="stylesheet" type="text/css" href="../../public/css/puzzles/j.css" >
<?php endblock() ?>

<?php startblock('content') ?>

<div class="enigma">
    
</div>
<?php endblock() ?>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="../../public/js/puzzles/puzzle_j.js"></script>
<script type="text/javascript">

$(function(){
    $(".enigma").puzzle();
});

</script>