<?php

include "../default/layout.php";
?>

<?php startblock('stylesheet') ?>
<link rel="stylesheet" type="text/css" href="../../public/css/puzzles/noel.css" >
  <script src="../../public/js/puzzles/enigmanoel.js"></script>
	<script src="../../public/js/puzzles/funcionesenigmanoel.js""></script>
<?php endblock() ?>

<?php startblock('content') ?>

	<h2 class="texto"> En la India , Ibn Batutta fue nombrado ‘qadi’ (Juez) por  el sultán <b><u>Muhammad</b></u>.
A Ibn le ofrecieron ir de embajador hacia China , pero durante el viaje en barco , recibió ataques de hindúes que le hicieron abortar la misión , y además , debido a una tormenta los barcos que el llevaba se hundieron en el agua, con lo que por miedo de volver a ver al sultán Muhammad con un fracaso de este tipo , tuvo que retirarse de la India. 
   </h2>
	<button  type="button"  class="btn btn-default inicio">Iniciar enigma </button>

<div class="hidden" id="ocult">
	<h1 class="texto">Utiliza el nombre del  sultán para resolver el enigma.</h1>

	    <div class="container">
	<div class="row">
		<div class="col-md-4">
			<button type="submit"  class="boton_1" data-value="1">1</button>
		</div>
		
		<div class="col-md-4">
			<button type="submit" class="boton_1" data-value="2" >2</button>
		</div>
		<div class="col-md-4">
			<button type="submit" class="boton_1" data-value="3">3</button>
			</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<button type="submit" class="boton_1" data-value="4">4</button>
		</div>
		<div class="col-md-4">
			<button type="submit" class="boton_1" data-value="5">5</button>
		</div>
		<div class="col-md-4">
			<button type="submit" class="boton_1" data-value="6">6</button>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<button type="submit" class="boton_1" data-value="7">7</button>
		</div>
		<div class="col-md-4">
			<button type="submit" class="boton_1" data-value="8">8</button>
		</div>
		<div class="col-md-4"> 
			<button type="submit" class="boton_1" data-value="9">9</button>

		</div>

	</div>
	<div class="row">
		<div class="col-md-4">
			
		</div>
		<div class="col-md-4">
			<button type="submit" class="boton_1" data-value="0">0</button>
		</div>
		<input type="text" disabled
		 class="text" placeholder="Números" id="texto" /> 
		<div class="col-md-4">

		<button type="submit" class="btn btn-primary retro" data-value="retro">Retroceso</button>
		 <button type="submit" class="btn btn-primary enviar">Enviar
	      </button>
			
		</div>
	</div>
</div>
</div>

<?php endblock() ?>