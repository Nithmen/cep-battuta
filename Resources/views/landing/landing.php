<?php
include "../default/layout.php";
?>

<?php startblock('sidebar') ?>

<?php endblock() ?>

<?php startblock('content') ?>
<div class="container-fluid landing">

    <div class="row">
        <div class="col-xs-12">

            <div class="producte_estrella">
                <img class="img-responsive" src="../../public/img/layout/santasofia.jpg">

                <div class="producte_estrella_content">
                    ¿Estás preparado para el primer Escape Room sobre la vida de Ibn Battuta?
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <!-- WIDGET #1  -->
        <div class="col-xs-4">
            <div class="panel">
                <div class="panel-heading panel-landing">

                </div>
                <div class="panel-body">

                </div>
            </div>
        </div>
        <!-- END WIDGET #1 -->

        <!-- WIDGET #2  -->
        <div class="col-xs-4">
            <div class="panel">
                <div class="panel-heading panel-landing">

                </div>
                <div class="panel-body">

                </div>
            </div>
        </div>
        <!-- END WIDGET #2 -->

        <!-- WIDGET #3  -->
        <div class="col-xs-4">
            <div class="panel">
                <div class="panel-heading panel-landing">

                </div>
                <div class="panel-body">

                </div>
            </div>
        </div>
        <!-- END WIDGET #3 -->
    </div>
</div>

<?php endblock() ?>