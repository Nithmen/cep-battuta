<div class="row">
    <div class="col-sm-3 col-sm-offset-5 col-md-3">
        <div class="register_form">
            <form class="form" method="POST">
                <div class="form-group">
                    <label>Nom</label>
                    <input type="text" class="form-control" placeholder="Nom" />
                </div>
                
                <div class="form-group">
                    <label>Correu electrònic</label>
                    <input type="email" class="form-control" />
                </div>
                
                <div class="form-group">
                    <label>Contrasenya</label>
                    <input type="password" class="form-control" />
                </div>
                
                <div class="form-group">
                    <input type="submit" class="form-control btn-primary" value="Registrarme" />
                </div>
            </form>
        </div>
    </div>
</div>