<?php
    include "../default/layout.php";
//    Database::addUser("John", "jonathan.diaz@fandroid.es", "1234");
?>

<?php startblock('stylesheet') ?>
    <link rel="stylesheet" type="text/css" href="../../public/css/register/register.css">
<?php endblock() ?>

<?php startblock('content') ?>
    <div class="container">
        <?php include "../form/Register/form_register.php" ?>
    </div>
<?php endblock() ?>