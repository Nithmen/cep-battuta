<?php
require_once ($_SERVER['DOCUMENT_ROOT'] . '/cep-battuta/Resources/public/library/ti.php');

include_once ($_SERVER['DOCUMENT_ROOT'] . '/cep-battuta/Resources/public/library/Database.php');

session_start();
?>

<!DOCTYPE HTML>
<html>
    <head>
        <?php startblock('title') ?>
        <title>Ibn Battuta</title>
        <?php endblock() ?>

        <meta charset="utf-8">

        <!-- BootStrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<<<<<<< HEAD
        <script src="../../public/js/default.js"></script>

=======
>>>>>>> release/v1
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        
        <?php startblock('stylesheet') ?>
        <!-- CSS  -->
        <?php endblock() ?>

        <link rel="stylesheet" type="text/css" href="../../public/css/default/layout.css">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    </head>ja
    <body>
        <div class="canvas">
            <div class="header">
                <nav class="navbar affix-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <ul class="sub-nav">
                                    <li>
                                        <a href="#" class="btn btn-flat primary ripplelink">Login</a>
                                    </li>
                                    <li>
                                        <a href="#" class="btn btn-flat secondary ripplelink">Regístrate</a>
                                    </li>
                                </ul>   
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu" aria-expanded="false">
                                        <span class="sr-only">Menu</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <div class="navbar-header-logo">

                                    </div>
                                </div>

                                <div class="collapse navbar-collapse" id="menu">
                                    <ul class="nav navbar-nav">
                                        <li>
                                            <a href="#">Índice</a>
                                        </li>
                                        <li>
                                            <a href="#">Jugar</a>
                                        </li>
                                    </ul>
                                </div>


                            </div>
                        </div>
                    </div>
                </nav>
            </div>


            <div class="container">
                <?php startblock('sidebar') ?>
                <div class="col-md-3">
                    <div class="sidebar">
                        <div class="sidebar_header">
                            Menu
                        </div>

                        <div class="sidebar_menu">
                            <ul>
                                <li>
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                    Pistes
                                </li>
                                <li>
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                    Mapa
                                </li>
                                <li>
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                    Cromos
                                </li>
                                <li>
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                    Insertar codi
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php endblock() ?>


                <div class="row">
                    <div class="col-md-12">
                        <div class="content">
                            <?php startblock('content') ?>
                            <?php endblock() ?>
                        </div>
                    </div>

                </div>
            </div>
            <footer class="footer">
            <div class="footer_info">
                Copyright 2017 CENTRE D’ESTUDIS POLITÈCNICS. Tots els drets reservats.
            </div>
        </footer>
        </div>
        
    </body>
</html>