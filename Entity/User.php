<?php

/**
 * User Class
 */
class User
{
    /**
     * @var integer
     */
    protected $id;
    
    /**
     * @var string
     */
    protected $username;
    
    /**
     * @var string
     */
    protected $password;
    
    /**
     * @var string
     */
    protected $email;
    
    /**
     * Token per saber on es troba l'usuari actualment.
     * 
     * @var string
     */
    protected $token;
    
    /**
     *
     * @var Terminal
     */
    protected $terminal;
    
    /**
     *
     * @var Role
     */
    protected $role;
    
    public function __construct($id, $username, $password, $email, $role, $terminal = null, $token = null)
    {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
        $this->email = $email;
        $this->role = $role;
        $this->terminal = $terminal;
        $this->token = $token;
    }
    
    function getId()
    {
        return $this->id;
    }

    function getUsername()
    {
        return $this->username;
    }

    function getEmail()
    {
        return $this->email;
    }

    function getToken()
    {
        return $this->token;
    }

    function getTerminal()
    {
        return $this->terminal;
    }

    function getRole()
    {
        return $this->role;
    }

    function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    function setToken($token = null)
    {
        $this->token = $token;
        return $this;
    }

    function setTerminal(Terminal $terminal = null)
    {
        $this->terminal = $terminal;
        return $this;
    }

    function setRole(Role $role)
    {
        $this->role = $role;
        return $this;
    }
}
