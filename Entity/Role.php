<?php

/**
 * Role Class
 */
class Role
{
    const ROLE_ADMIN = 1;
    const ROLE_USER = 2;
    
    /**
     * @var integer
     */
    protected $id;
    
    /**
     * @var string
     */
    protected $name;
    
    /**
     * Constructor de la classe.
     * 
     * @param type $id
     * @param type $name
     */
    public function __construct($id, $name)
    {
       $this->id = $id;
       $this->name = $name;
    }
    
    function getId()
    {
        return $this->id;
    }

    function getName()
    {
        return $this->name;
    }

    function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    function setName($name)
    {
        $this->name = $name;
        return $this;
    }
}
